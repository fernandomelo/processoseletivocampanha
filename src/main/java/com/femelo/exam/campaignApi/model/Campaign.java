package com.femelo.exam.campaignApi.model;

import java.util.ArrayList;
import java.util.List;

public class Campaign extends BaseModel {
	
	private String name;
	private String startDate;
	private String endDate;
	private String teamId;
	private List<String> customers;

	public Campaign() {
		this.customers = new ArrayList<String>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public List<String> getCustomers() {
		return customers;
	}

	public void addCustomer(String customer) {
		if (!this.customers.contains(customer))
			this.customers.add(customer);
	}
}
