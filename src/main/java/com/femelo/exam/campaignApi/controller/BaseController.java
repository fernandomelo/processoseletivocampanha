package com.femelo.exam.campaignApi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.femelo.exam.campaignApi.model.BaseModel;

@RestController
public interface BaseController<T extends BaseModel> {
	
	public abstract ResponseEntity<T> create(T entity);
	
	public abstract ResponseEntity<T> update(String id, T entity);
	
	public abstract ResponseEntity<T> delete(String id);
	
	public abstract ResponseEntity<T> get(String id);
	
}