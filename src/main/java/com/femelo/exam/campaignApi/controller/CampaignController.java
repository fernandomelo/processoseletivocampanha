package com.femelo.exam.campaignApi.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.femelo.exam.campaignApi.business.CampaignBusiness;
import com.femelo.exam.campaignApi.helper.CampaignValidationException;
import com.femelo.exam.campaignApi.helper.JsonMessage;
import com.femelo.exam.campaignApi.model.Campaign;

@RestController
@RequestMapping(value = "/campaign")
public class CampaignController implements BaseController<Campaign> {
	
	@Autowired
	private CampaignBusiness campaignBusiness;
	
	public CampaignController(CampaignBusiness campaignBusiness) {
		this.campaignBusiness = campaignBusiness;
	}
	
	@Override
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<Campaign> create(@RequestBody Campaign entity) {
		Campaign inserted = null;
		try {
			inserted = campaignBusiness.create(entity);
		} catch (CampaignValidationException e) {
			return badRequest();
		}
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Location", "/campaign/" + inserted.getId());
		
		return new ResponseEntity<Campaign>(inserted, headers, HttpStatus.CREATED);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Campaign> update(@PathVariable("id") String id, @RequestBody Campaign entity) {
		Campaign c = null;
		try {
			c = campaignBusiness.update(id, entity);
		} catch (CampaignValidationException e) {
			return badRequest();
		}
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Campaign>(c, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Campaign> delete(@PathVariable("id") String id) {
		Campaign c = campaignBusiness.delete(id);
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Campaign>(c, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Campaign> get(@PathVariable("id") String id) {
		Campaign c = campaignBusiness.findById(id);
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Campaign>(c, HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<Page<Campaign>> getActive(Pageable pageable) {
		Page<Campaign> list = campaignBusiness.findActive(pageable);
		
		return new ResponseEntity<Page<Campaign>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/team/{teamId}", method = RequestMethod.GET)
	public ResponseEntity<Page<Campaign>> getActiveByTeamId(@PathVariable("teamId") String teamId, Pageable pageable) {
		Page<Campaign> list = campaignBusiness.findActiveByTeamId(teamId, pageable);
		
		return new ResponseEntity<Page<Campaign>>(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/modified", method = RequestMethod.GET)
	public ResponseEntity<Page<Campaign>> getModifiedCampaigns(@RequestParam("reference") String reference, Pageable pageable) {
		Page<Campaign> list = campaignBusiness.findModifiedCampaigns(reference, pageable);
		
		return new ResponseEntity<Page<Campaign>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/created", method = RequestMethod.GET)
	public ResponseEntity<Page<Campaign>> getCreatedCampaigns(@RequestParam("reference") String reference, Pageable pageable) {
		Page<Campaign> list = campaignBusiness.findCreatedCampaigns(reference, pageable);
		
		return new ResponseEntity<Page<Campaign>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}/link", method = RequestMethod.POST)
	public ResponseEntity<Campaign> linkCustomerAndCampaign(@PathVariable("id") String id, @RequestParam("customerId") String customerId) {
		Campaign c = campaignBusiness.addCustomerToCampaign(id, customerId);
		
		if (Objects.isNull(c)) {
			return notFound(id);
		}
		
		return new ResponseEntity<Campaign>(c, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Campaign> notFound(String id) {
		JsonMessage message = new JsonMessage();
		message.setMessage("Could not find Campaign '" + id + "'.");
		message.setStatus("WARNING");
		
		return new ResponseEntity(message, HttpStatus.NOT_FOUND);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ResponseEntity<Campaign> badRequest() {
		JsonMessage message = new JsonMessage();
		message.setMessage("Could not create or update Campaign. One or more fields are invalid.");
		message.setStatus("ERROR");
		
		return new ResponseEntity(message, HttpStatus.BAD_REQUEST);
	}
}
