package com.femelo.exam.campaignApi.business;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.femelo.exam.campaignApi.helper.CampaignValidationException;
import com.femelo.exam.campaignApi.model.Campaign;
import com.femelo.exam.campaignApi.repository.CampaignRepository;

@Component
public class CampaignBusiness {
	
	@Autowired
	private CampaignRepository campaignRepository;
	
	public CampaignBusiness(CampaignRepository campaignRepository) {
		this.campaignRepository = campaignRepository;
	}
	
	public Campaign create(Campaign entity) throws CampaignValidationException {
		entity.setCreatedDate(Instant.now().toString());
		entity.setId(null);
		entity.setModifiedDate(null);
		
		if (!validateModel(entity))
			throw new CampaignValidationException("Campaign is invalid or incomplete.");
		
		List<Campaign> concurrent = incrementEndDates(entity);
		concurrent.add(0, entity);
		
		checkIfDateIsTakenAndReleaseIt(concurrent);
		
		Campaign created = null;
		
		for(int i = 0; i < concurrent.size(); i++) {
			Campaign x = concurrent.get(i);
			if (x.getId() == null) {
				created = campaignRepository.insert(entity);
			}
			else {
				x.setModifiedDate(Instant.now().toString());
				campaignRepository.save(x);
			}
		}

		return created;
	}
	
	private void checkIfDateIsTakenAndReleaseIt(List<Campaign> list) {
		if (list == null || list.size() == 0)
			return;
		
		Campaign current = list.get(0);
		
		list.subList(1, list.size()).forEach(x -> {
			if (current.getEndDate().equals(x.getEndDate()))
				x.setEndDate(incrementedEndDate(x));
		});
		
		checkIfDateIsTakenAndReleaseIt(list.subList(1, list.size()));
	}

	private List<Campaign> incrementEndDates(Campaign entity) {
		List<Campaign> concurrent = campaignRepository.findByInterval(entity.getStartDate(), entity.getEndDate());
		concurrent.sort((x, y) -> x.getStartDate().compareTo(y.getStartDate()));
		concurrent.sort((x, y) -> x.getEndDate().compareTo(y.getEndDate()));
		
		concurrent.forEach(x -> x.setEndDate(incrementedEndDate(x)));
		return concurrent;
	}

	private String incrementedEndDate(Campaign x) {
		String year = x.getEndDate().split("-")[0];
		String month = x.getEndDate().split("-")[1];
		String day = x.getEndDate().split("-")[2];
		
		ZonedDateTime dt = ZonedDateTime.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), 0, 0, 0, 0, ZoneOffset.UTC);
		
		dt = dt.plus(1, ChronoUnit.DAYS);
		
		return ZonedDateTime.ofInstant(dt.toInstant(), ZoneOffset.UTC).format(Campaign.DATE_STRING_PATTERN);
	}

	public Campaign update(String id, Campaign entity) throws CampaignValidationException {
		Campaign c = campaignRepository.findOne(id);
		
		if (Objects.isNull(c))
			return null;
		
		c.setId(id);
		c.setName(entity.getName());
		c.setTeamId(entity.getTeamId());
		c.setStartDate(entity.getStartDate());
		c.setEndDate(entity.getEndDate());
		c.setModifiedDate(Instant.now().toString());
		
		if (!validateModel(c))
			throw new CampaignValidationException("Campaign is invalid or incomplete.");
		
		c = campaignRepository.save(c);
		
		return c;
	}
	
	public Campaign delete(String id) {
		Campaign c = campaignRepository.findOne(id);
		
		if (Objects.nonNull(c))
			campaignRepository.delete(id);
		
		return c;
	}
	
	public Campaign findById(String id) {
		return campaignRepository.findOne(id);
	}
	
	public Page<Campaign> findActive(Pageable pageable) {
		Page<Campaign> list = campaignRepository.findByEndDate(Campaign.today(), pageable);
		
		return list;
	}
	
	public Page<Campaign> findActiveByTeamId(String teamId, Pageable pageable) {
		Page<Campaign> list = campaignRepository.findByEndDateAndTeamId(Campaign.today(), teamId, pageable);
		
		return list;
	}
	
	public Page<Campaign> findCreatedCampaigns(String reference, Pageable pageable) {
		Page<Campaign> list = campaignRepository.findCreatedCampaigns(reference, pageable);
		
		return list;
	}
	
	public Page<Campaign> findModifiedCampaigns(String reference, Pageable pageable) {
		Page<Campaign> list = campaignRepository.findModifiedCampaigns(reference, (pageable));
		
		return list;
	}
	
	public Campaign addCustomerToCampaign(String id, String customerId) {
		Campaign c = campaignRepository.findOne(id);
		
		if (Objects.isNull(c)) {
			return null;
		}
		
		c.addCustomer(customerId);
		c = campaignRepository.save(c);
		
		return c;
	}
	
	private boolean validateModel(Campaign entity) {
		if (Objects.isNull(entity)
				|| entity.getStartDate() == null || entity.getStartDate().isEmpty()
				|| entity.getEndDate() == null || entity.getEndDate().isEmpty()
				|| entity.getName() == null || entity.getName().isEmpty()
				|| entity.getTeamId() == null || entity.getTeamId().isEmpty()
				|| entity.getStartDate().compareTo(entity.getEndDate()) > 0)				
			return false;
		
		return true;
	}
	
}
