package com.femelo.exam.campaignApi.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.femelo.exam.campaignApi.model.Campaign;

public interface CampaignRepository extends MongoRepository<Campaign, String> {
	
	@Override
    @Cacheable("campaignPages")
	public Page<Campaign> findAll(Pageable pageable);
	
	public Page<Campaign> findByTeamId(String teamId, Pageable pageable);
	
	@Query("{ 'modifiedDate' : { $gte: ?0 } }")
	Page<Campaign> findModifiedCampaigns(String referenceDate, Pageable pageable);
	
	@Query("{ 'createdDate' : { $gte: ?0 } }")
	Page<Campaign> findCreatedCampaigns(String referenceDate, Pageable pageable);
	
	@Query("{ 'endDate' : { $gte: ?0 }, 'teamId' : ?1 }")
	public Page<Campaign> findByEndDateAndTeamId(String endDate, String teamId, Pageable pageable);
	
	@Query("{ 'endDate' : { $gte: ?0 } }")
	public Page<Campaign> findByEndDate(String endDate, Pageable pageable);

	@Query("{ 'endDate' : { $gte: ?0 }, 'startDate' : { $lte: ?1 } }")
	public List<Campaign> findByInterval(String startDate, String endDate);
}