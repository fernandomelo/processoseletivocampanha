package com.femelo.exam.campaignApi.helper;

public class CampaignValidationException extends Exception {

	public CampaignValidationException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -2888951492544399696L;

}
