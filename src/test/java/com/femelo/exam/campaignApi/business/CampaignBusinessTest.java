package com.femelo.exam.campaignApi.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.femelo.exam.campaignApi.helper.CampaignValidationException;
import com.femelo.exam.campaignApi.model.Campaign;
import com.femelo.exam.campaignApi.repository.CampaignRepository;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles(profiles = "test")
public class CampaignBusinessTest {
	
	CampaignBusiness campaignBusiness;
	
	@Mock
	private CampaignRepository campaignRepository;
	
	private static String VALID_CAMPAIGN_NAME = "Campanha 01";
	private static String INVALID_CAMPAIGN_NAME = "";
	
	private static String VALID_CAMPAIGN_START = "2017-07-01";
	private static String INVALID_CAMPAIGN_START = "";
	
	private static String VALID_CAMPAIGN_END = "2017-07-10";
	private static String INVALID_CAMPAIGN_END = "";
	
	private static String INVALID_CAMPAIGN_START_REVERSE = "2017-07-10";
	private static String INVALID_CAMPAIGN_END_REVERSE = "2017-07-01";
	
	private static String VALID_CAMPAIGN_TEAMID = "12993";
	private static String INVALID_CAMPAIGN_TEAMID = "";
	
	private Page<Campaign> fakeFullPage;
	private Page<Campaign> fakeEmptyPage;
	
    @Before
    public void setUp() {
        campaignBusiness = new CampaignBusiness(campaignRepository);
        
        fakeFullPage = new PageImpl<Campaign>(Arrays.asList(new Campaign(), new Campaign()));
        fakeEmptyPage = new PageImpl<Campaign>(Arrays.asList());
    }
	
	@Test
	public void shouldValidateSuccessModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
		} catch (CampaignValidationException e) {
			e.printStackTrace();
			fail("Should have inserted the Campaign");
		}
		
		//then
		verify(campaignRepository, times(1)).insert(any(Campaign.class));
	}
	
	@Test
	public void shouldInvalidateEmptyNameModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(INVALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
			
			fail("Should not have inserted the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyStartModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(INVALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
			
			fail("Should not have inserted the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyEndModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(INVALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
			
			fail("Should not have inserted the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyTeamIdModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(INVALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
			
			fail("Should not have inserted the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateStartGreaterThenEndModelOnCreating() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(INVALID_CAMPAIGN_START_REVERSE);
		c.setEndDate(INVALID_CAMPAIGN_END_REVERSE);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		try {
			campaignBusiness.create(c);
			
			fail("Should not have inserted the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void shouldValidateSuccessModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
		} catch (CampaignValidationException e) {
			e.printStackTrace();
			fail("Should have udpdated the Campaign");
		}
		
		//then
		verify(campaignRepository, times(1)).findOne("1");
		verify(campaignRepository, times(1)).save(any(Campaign.class));
	}
	
	@Test
	public void shouldInvalidateEmptyNameModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(INVALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
			
			fail("Should not have udpdated the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyStartModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(INVALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
			
			fail("Should not have udpdated the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyEndModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(INVALID_CAMPAIGN_END);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
			
			fail("Should not have udpdated the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateEmptyTeamIdModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(VALID_CAMPAIGN_START);
		c.setEndDate(VALID_CAMPAIGN_END);
		c.setTeamId(INVALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
			
			fail("Should not have udpdated the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldInvalidateStartGreaterThenEndModelOnEditing() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(INVALID_CAMPAIGN_START_REVERSE);
		c.setEndDate(INVALID_CAMPAIGN_END_REVERSE);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());
		try {
			campaignBusiness.update("1", c);
			
			fail("Should not have udpdated the Campaign");
		} catch (CampaignValidationException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void shouldReturnNullCaseIdIsNotFound() {
		//given
		Campaign c = new Campaign();
		c.setName(VALID_CAMPAIGN_NAME);
		c.setStartDate(INVALID_CAMPAIGN_START_REVERSE);
		c.setEndDate(INVALID_CAMPAIGN_END_REVERSE);
		c.setTeamId(VALID_CAMPAIGN_TEAMID);
		
		//when
		when(campaignRepository.findOne("1")).thenReturn(null);
		try {
			Campaign update = campaignBusiness.update("1", c);
			
			assertNull(update);
			
		} catch (CampaignValidationException e) {
			e.printStackTrace();
			fail("Should not have thrown exception.");
		}
	}
	
	@Test
	public void shouldDeleteCampaign() {
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());

		campaignBusiness.delete("1");
		
		//then
		verify(campaignRepository, times(1)).findOne("1");
		verify(campaignRepository, times(1)).delete("1");
	}

	@Test
	public void shouldNotCallDeleteIfCampaignIsNotFound() {
		//when
		when(campaignRepository.findOne("1")).thenReturn(null);

		Campaign c = campaignBusiness.delete("1");
		
		//then
		verify(campaignRepository, times(1)).findOne("1");
		verify(campaignRepository, times(0)).delete("1");
		assertNull(c);
	}
	
	@Test
	public void shouldFindCampaignById() {
		//when
		when(campaignRepository.findOne("1")).thenReturn(new Campaign());

		Campaign c = campaignBusiness.findById("1");
		
		//then
		verify(campaignRepository, times(1)).findOne("1");
		assertNotNull(c);
	}

	@Test
	public void shouldNotFindCampaignById() {
		//when
		when(campaignRepository.findOne("1")).thenReturn(null);

		Campaign c = campaignBusiness.findById("1");
		
		//then
		verify(campaignRepository, times(1)).findOne("1");
		assertNull(c);
	}
	
	@Test
	public void shouldFindActiveCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findByEndDate(Campaign.today(), pageRequest))
			.thenReturn(fakeFullPage);
		
		Page<Campaign> list = campaignBusiness.findActive(pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(2, list.getContent().size());
		verify(campaignRepository, times(1)).findByEndDate(Campaign.today(), pageRequest);
	}

	@Test
	public void shouldNotFindActiveCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findByEndDate(any(String.class), (Pageable) any(Pageable.class)))
			.thenReturn(fakeEmptyPage);
		
		Page<Campaign> list = campaignBusiness.findActive(pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(list.getSize(), 0);
		verify(campaignRepository, times(1)).findByEndDate(any(String.class), (Pageable) any(Pageable.class));
	}
	
	@Test
	public void shouldFindActiveCampaignsByTeamId() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findByEndDateAndTeamId(Campaign.today(), "01", pageRequest))
			.thenReturn(fakeFullPage);
		
		Page<Campaign> list = campaignBusiness.findActiveByTeamId("01", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(2, list.getContent().size());
		verify(campaignRepository, times(1)).findByEndDateAndTeamId(Campaign.today(), "01", pageRequest);
	}

	@Test
	public void shouldNotFindActiveCampaignsByTeamId() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findByEndDateAndTeamId(Campaign.today(), "01", pageRequest))
			.thenReturn(fakeEmptyPage);
		
		Page<Campaign> list = campaignBusiness.findActiveByTeamId("01", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(list.getSize(), 0);
		verify(campaignRepository, times(1)).findByEndDateAndTeamId(any(String.class), eq("01"), (Pageable) any(Pageable.class));
	}
	
	@Test
	public void shouldFindCreatedCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest))
			.thenReturn(fakeFullPage);
		
		Page<Campaign> list = campaignBusiness.findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(2, list.getContent().size());
		verify(campaignRepository, times(1)).findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
	}

	@Test
	public void shouldNotFindCreatedCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest))
			.thenReturn(fakeEmptyPage);
		
		Page<Campaign> list = campaignBusiness.findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(0, list.getContent().size());
		verify(campaignRepository, times(1)).findCreatedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
	}
	
	@Test
	public void shouldFindModifiedCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest))
			.thenReturn(fakeFullPage);
		
		Page<Campaign> list = campaignBusiness.findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(2, list.getContent().size());
		verify(campaignRepository, times(1)).findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
	}

	@Test
	public void shouldNotFindModifiedCampaigns() {
		//given
		PageRequest pageRequest = new PageRequest(0, 20);
		
		//when
		when(campaignRepository.findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest))
			.thenReturn(fakeEmptyPage);
		
		Page<Campaign> list = campaignBusiness.findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
		
		//then
		assertNotNull(list);
		assertEquals(0, list.getContent().size());
		verify(campaignRepository, times(1)).findModifiedCampaigns("2017-06-12T02:57:00.616Z", pageRequest);
	}
	
	public void shouldNotAddCustumerToCampaignIfCampaignIdNotFound() {
		String customerId = "333";
		String campaignId = "222";
		
		//when
		when(campaignRepository.findOne(campaignId)).thenReturn(null);

		Campaign c = campaignBusiness.addCustomerToCampaign(campaignId, customerId);
		
		//then
		verify(campaignRepository, times(1)).findOne(customerId);
		assertNull(c);
	}
	
	public void shouldAddCustumerToCampaign() {
		String customerId = "333";
		String campaignId = "222";
		Campaign campaign = new Campaign();
		
		//when
		when(campaignRepository.findOne(campaignId)).thenReturn(campaign);

		Campaign c = campaignBusiness.addCustomerToCampaign(campaignId, customerId);
		
		//then
		verify(campaignRepository, times(1)).findOne(customerId);
		assertNotNull(c);
		assertEquals(1, c.getCustomers().size());
	}
	
	public void shouldAddTwoCustumerToCampaign() {
		String customerId = "333";
		String customerId2 = "444";
		String campaignId = "222";
		Campaign campaign = new Campaign();
		
		//when
		when(campaignRepository.findOne(campaignId)).thenReturn(campaign);

		Campaign c = campaignBusiness.addCustomerToCampaign(campaignId, customerId);
		c = campaignBusiness.addCustomerToCampaign(campaignId, customerId2);
		
		//then
		verify(campaignRepository, times(2)).findOne(customerId);
		assertNotNull(c);
		assertEquals(2, c.getCustomers().size());
	}
	
	public void shouldNotDuplicateCustomerInCampaign() {
		String customerId = "333";
		String campaignId = "222";
		Campaign campaign = new Campaign();
		
		//when
		when(campaignRepository.findOne(campaignId)).thenReturn(campaign);

		Campaign c = campaignBusiness.addCustomerToCampaign(campaignId, customerId);
		c = campaignBusiness.addCustomerToCampaign(campaignId, customerId);
		
		//then
		verify(campaignRepository, times(2)).findOne(customerId);
		assertNotNull(c);
		assertEquals(1, c.getCustomers().size());
	}
	
	public void shouldIncrementDatesProperly() {
		//Given
		Campaign campaign01 = new Campaign();
		campaign01.setName("Teste");
		campaign01.setTeamId("01");
		campaign01.setStartDate("2017-06-01");
		campaign01.setEndDate("2017-06-10");
		
		Campaign campaign02 = new Campaign();
		campaign02.setStartDate("2017-06-01");
		campaign02.setEndDate("2017-06-09");
		
		Campaign campaign03 = new Campaign();
		campaign03.setStartDate("2017-06-01");
		campaign03.setEndDate("2017-06-09");
		
		List<Campaign> list = Arrays.asList(campaign02, campaign01);
		
		//when
		when(campaignRepository.findByInterval(campaign03.getStartDate(), campaign03.getEndDate()))
			.thenReturn(list);
		
		
		//then
		verify(campaignRepository, times(1)).findByInterval(eq("2017-06-01"), eq("2017-06-09"));
		verify(campaignRepository, times(1)).insert(any(Campaign.class));
		verify(campaignRepository, times(2)).save(any(Campaign.class));
		assertEquals("2017-06-11", campaign01.getEndDate());
		assertEquals("2017-06-10", campaign02.getEndDate());
		assertEquals("2017-06-09", campaign03.getEndDate());
	}
}
