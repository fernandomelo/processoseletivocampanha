package com.femelo.exam.campaignApi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.femelo.exam.campaignApi.controller.CampaignController;
import com.femelo.exam.campaignApi.repository.CampaignRepository;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class CampaignApiApplicationTest {

	@Autowired
    private CampaignController campaignController;
	
	@Autowired
    private CampaignRepository campaignRepository;
	
	@Test
	public void contextLoads() {
		assertThat(campaignController).isNotNull();
		assertThat(campaignRepository).isNotNull();
	}

}
