# README #

API de Campanhas para o teste prático do processo seletivo (Pergunta 01).

### O que a API faz? ###

É uma API para gerenciamento de Campanhas, com todas as funções de CRUD. Ela recebe e responde apenas requisições JSON.
As informações são persistidas em uma base de dados MongoDB.

O requisito funcional de atender a 100 chamadas por segundo não é atendido para a criação das campanhas. Conseguimos atingir 44 criações por segundo, com tempo médio de 1.7 segundo. Vale ressaltar que o ambiente em que foi rodado era composto por máquinas totalmente Sandbox (Heroku para o app e mLab para MongoDB).

Já para consultas, foi possível bater as 100 chamadas por segundo, atingindo uma média de tempo de resposta de 680ms. Nume segunda tentativa, se aproveitando melhor dos caches, foi atingida a média 438ms, atingindo 211 requisições por segundo.


### Como testar? ###

O projeto inclui um arquivo do Postman, para facilitar os testes manuais da API. (na pasta raiz)

Também há dois arquivos do JMeter com os testes que foram realizados de criação de campanhas e consulta de listas, juntamente com arquivos de massa de testes. (na pasta raiz) 

Existe uma versão hospedada no Heroku, na seguinte URL:

https://sheltered-mountain-81087.herokuapp.com/

** O primeiro request pode demorar, pois a máquina é desativada depois de 30 minutos de inatividade **

Para rodar a API numa máquina local, é necessário editar o application.properties para o perfil "local".
A aplicação vai apontar para uma instância local padrão do MongoDB.

As dependências do projeto são gerenciadas via Maven.
O projeto foi iniciado com um esqueleto do Spring Boot (http://start.spring.io/), com dependências Web, Cache e MongoDB.

### Testes unitários ###

Foram desenvolvidos testes unitários na camada de negócios da aplicação. Tenho executado eles com o JUnit para garantir a integridade das regras. (Com Mocks para a base/repositórios)